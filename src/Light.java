import Basic.Vec4;
import com.jogamp.opengl.GL3;

public class Light {
    private GL3 gl;

    private int shader = 0;

    private float[] position = {0.0f, 0.0f, 0.0f, 0.0f};

    private Vec4 lightAmbient = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
    private Vec4 lightDiffuse = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
    private Vec4 lightSpecular = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);

    private Vec4 matAmbient = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
    private Vec4 matDiffuse = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
    private Vec4 matSpecular = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
    private float matShininess = 0.0f;

    public Light(GL3 gl3, int newShader, float x, float y, float z, float w) {
        //accept various params relating to the light be passed in the constructor
        //this includes a reference to the shader in use and the light position due to it only being setup to support point lights
        gl = gl3;
        shader = newShader;

        position[0] = x;
        position[1] = y;
        position[2] = z;
        position[3] = w;
    }

    public void setupLight() {

        Vec4 ambientProduct = lightAmbient.times(matAmbient);
        float[] ambient = ambientProduct.getVector();
        Vec4 diffuseProduct = lightDiffuse.times(matDiffuse);
        float[] diffuse = diffuseProduct.getVector();
        Vec4 specularProduct = lightSpecular.times(matSpecular);
        float[] specular = specularProduct.getVector();

        //setup uniform variables for use within the vertex shader
        gl.glUseProgram(shader);
        gl.glUniform4fv( gl.glGetUniformLocation(shader, "AmbientProduct"),
                1, ambient,0 );
        gl.glUniform4fv( gl.glGetUniformLocation(shader, "DiffuseProduct"),
                1, diffuse, 0 );
        gl.glUniform4fv( gl.glGetUniformLocation(shader, "SpecularProduct"),
                1, specular, 0 );

        gl.glUniform4fv( gl.glGetUniformLocation(shader, "LightPosition"),
                1, position, 0 );

        gl.glUniform1f( gl.glGetUniformLocation(shader, "Shininess"),
                matShininess );
        gl.glUseProgram(0);
    }

    public void setShaderProgram(int newShader) {
        shader = newShader;
    }

    public void setLightPosition(float x, float y, float z, float w) {
        position[0] = x;
        position[1] = y;
        position[2] = z;
        position[3] = w;
    }

    //accessor methods allow the Source class to modify the light params
    public void setLightAmbient(Vec4 newLightAmbient) {
        lightAmbient = newLightAmbient;
    }

    public void setLightDiffuse(Vec4 newLightDiffuse) {
        lightDiffuse = newLightDiffuse;
    }

    public void setLightSpecular(Vec4 newLightSpecular) {
        lightSpecular = newLightSpecular;
    }

    public void setMatAmbient(Vec4 newMatAmbient) {
        matAmbient = newMatAmbient;
    }

    public void setMatDiffuse(Vec4 newMatDiffuse) {
        matDiffuse = newMatDiffuse;
    }

    public void setMatSpecular(Vec4 newMatSpecular) {
        matSpecular = newMatSpecular;
    }

    public void setMatShininess(float newMatShininess) {
        matShininess = newMatShininess;
    }
}
