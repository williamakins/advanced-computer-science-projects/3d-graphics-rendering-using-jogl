package Objects;

public class SVolume extends SObject {
    private float width = 1.0f;
    private float height = 1.0f;
    private float depth = 1.0f;

    public SVolume() {
        super();
        update();
    }

    public SVolume(float w, float h, float d) {
        super();
        width = w;
        height = h;
        depth = d;

        update();
    }

    @Override
    protected void genData() {
        numVertices = 24;
        numIndices = 36;

        vertices = new float[] {
                -width, -height, -depth,
                width, -height, -depth,
                width, height, -depth,
                -width, height, -depth,
                -width, -height, depth,
                width, -height, depth,
                width, height, depth,
                -width, height, depth
        };

        normals = new float[] {
                1.0f, 0.0f, 0.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 1.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 0.0f, 1.0f
        };

        textures = new float[] {
                1.0f, 0.0f, 0.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 1.0f, 1.0f,
                0.0f, 1.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 1.0f, 1.0f,
                1.0f, 0.0f, 0.0f, 1.0f
        };

        indices = new int[] {
                0, 1, 3,
                3, 1, 2,
                1, 5, 2,
                2, 5, 6,
                5, 4, 6,
                6, 4, 7,
                4, 0, 7,
                7, 0, 3,
                3, 2, 7,
                7, 2, 6,
                4, 5, 0,
                0, 5, 1
        };
    }
}