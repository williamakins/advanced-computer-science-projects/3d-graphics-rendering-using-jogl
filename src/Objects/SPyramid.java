package Objects;

public class SPyramid extends SObject {
    public SPyramid() {
        super();
        update();
    }

    @Override
    protected void genData() {
        numVertices = 12;
        numIndices = 12;

        vertices = new float[] {
                -0.5f, -1.0f, -0.85f,
                -0.5f, -1, 0.85f,
                1.0f, -1.0f, 0.0f,
                0.0f, 1.0f, 0.0f
        };

        normals = new float[] {
                0.0f, -1.0f, 0.0f,
                0.0f, -1.0f, 0.0f,
                0.0f, -1.0f, 0.0f,
                -1.0f, 0.25f, -0.0f,
                -1.0f, 0.25f, -0.0f,
                -1.0f, 0.25f, -0.0f,
                0.5f, 0.25f, 0.85f,
                0.5f, 0.25f, 0.85f,
                0.5f, 0.25f, 0.85f,
                0.5f, 0.25f, -0.85f,
                0.5f, 0.25f, -0.85f,
                0.5f, 0.25f, -0.85f
        };

        textures = new float[] {
                0.375f, 0.0f,
                0.4f, 0.5f,
                0.75f, 0.25f,
                0.25f, 0.5f,
                0.4f, 0.5f,
                0.6f, 0.5f,
                0.75f, 0.5f,
                0.5f, 1.0f
        };

        indices = new int[] {
                0, 1, 2,
                2, 1, 3,
                3, 2, 4,
                4, 3, 5
        };
    }
}