import static com.jogamp.opengl.GL.GL_ARRAY_BUFFER;
import static com.jogamp.opengl.GL.GL_CULL_FACE;
import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL3.*;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import Basic.ShaderProg;
import Basic.Transform;
import Basic.Vec3;
import Basic.Vec4;
import Objects.*;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

public class Source extends JFrame {
    //Base boilerplate code derived from the University lab sessions, with major overhaul taking place on top of it

    private final GLCanvas canvas; //Define a canvas
    private final FPSAnimator animator=new FPSAnimator(60, true);
    private final Source.Renderer renderer = new Source.Renderer();


    //Transformation parameters
    public static int ModelView;
    public static int NormalTransform;
    public static int Projection;

    //private GenerateObject teapot;
    //private GenerateObject sphere;

    public Source() {
        GLProfile glp = GLProfile.get(GLProfile.GL3);
        GLCapabilities caps = new GLCapabilities(glp);
        canvas = new GLCanvas(caps);

        add(canvas, java.awt.BorderLayout.CENTER); // Put the canvas in the frame
        canvas.addGLEventListener(renderer); //Set the canvas to listen GLEvents
        canvas.addMouseListener(renderer);
        canvas.addMouseMotionListener(renderer);
        canvas.addKeyListener(renderer);

        animator.add(canvas);

        setTitle("William Akins - Graphics Coursework");
        setSize(800,800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        animator.start();
        canvas.requestFocus();
    }

    public static void main(String[] args) {
        new Source();
    }

    class Renderer implements GLEventListener, MouseListener, MouseMotionListener, KeyListener {

        private Transform T = new Transform();

        private boolean showSphere = true;

        private float scale = 1;
        private float tx = 0;
        private float ty = 0;
        private float tz = 0;
        private float rx = 0;
        private float ry = 0;
        private float rz = 0;

//        //Mouse position
        private int xMouse = 0;
        private int yMouse = 0;

        private Object teapot;
        private Object sphere;
        private Object cube;

        int ShaderProgram;
        int ShaderProgram2;

        //texture parameters
        private ByteBuffer[] texImg = new ByteBuffer[3];
        //private intbuffer[] textureID = new int[2];
        private int texWidth, texHeight;
        private int texName[] = new int[3];

        @Override
        public void display(GLAutoDrawable drawable) {
            // Get the GL pipeline object this
            GL3 gl = drawable.getGL().getGL3();

            gl.glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            gl.glClearColor(0.5f,0f,0f, 1f);

            //set up the camera
            T.lookAt(0, 0, -2.0f, 0, 0, -100, 0, 1, 0); //default parameters

            //Send model_view and normal transformation matrices to shader.
            //Here parameter 'true' for transpose means to convert the row-major
            //matrix to column major one, which is required when vertices'
            //location vectors are pre-multiplied by the model_view matrix.
            //Note that the normal transformation matrix is the inverse-transpose
            //matrix of the vertex transformation matrix
            T.initialize();
            T.scale(scale, scale, scale);
            T.rotateX(rx);
            T.rotateY(ry);
            T.translate(tx, ty, tz);

            teapot.renderObject(gl, T, new Vec3(-2.5f,-0.5f,0), new Vec3(0.2f,0.2f,0.2f), new Vec3(0,0,0));

            cube.renderObject(gl, T, new Vec3(0.0f,-1.0f,-0.5f), new Vec3(1f,1f,1f), new Vec3(0,0,0));

            if (showSphere)
                sphere.renderObject(gl, T, new Vec3(1.5f,-0.5f,0), new Vec3(0.5f,0.5f,0.5f), new Vec3(0,0,0));


        }

        @Override
        public void dispose(GLAutoDrawable drawable) {
            // TODO Auto-generated method stub
        }

        @Override
        public void init(GLAutoDrawable drawable) {
            // Get the GL pipeline object this
            GL3 gl = drawable.getGL().getGL3();
            gl.glEnable(GL_CULL_FACE);

            //initalise the frag and vert shader for later use
            ShaderProg shaderproc = new ShaderProg(gl, "shader.vert", "shader.frag");
            ShaderProgram = shaderproc.getProgram();
            ShaderProgram2 = shaderproc.getProgram();

            //setup light properties for the first light, this also includes position in its constructor
            Light myLight = new Light(gl, ShaderProgram,100.0f, 100.0f, 100.0f, 0.0f);
            myLight.setLightAmbient(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
            myLight.setLightDiffuse(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
            myLight.setLightSpecular(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));

            myLight.setMatAmbient(new Vec4(0.329412f, 0.223529f, 0.027451f, 1.0f));
            myLight.setMatDiffuse(new Vec4(0.780392f, 0.568627f, 0.113725f, 1.0f));
            myLight.setMatSpecular(new Vec4(0.992157f, 0.941176f, 0.807843f, 1.0f));
            myLight.setMatShininess(27.8974f);

            //setup the light params and position for the second light
            Light myLight1 = new Light(gl, ShaderProgram2,20.0f, 100.0f, 100.0f, 0.0f);
            myLight1.setLightAmbient(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
            myLight1.setLightDiffuse(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
            myLight1.setLightSpecular(new Vec4(1.0f, 1.0f, 1.0f, 1.0f));

            myLight1.setMatAmbient(new Vec4(1.329412f, 0.223529f, 1.027451f, 1.0f));
            myLight1.setMatDiffuse(new Vec4(0.780392f, 1.568627f, 0.113725f, 1.0f));
            myLight1.setMatSpecular(new Vec4(1.992157f, 0.941176f, 1.807843f, 1.0f));
            myLight1.setMatShininess(97.8974f);

            //actually generate the light after all the properties are setup
            myLight.setupLight();

            //load and setup texture and define its parameters such as filtering mode
            texImg[0] = loadImage("ceramic.jpg");
            texName[0] = 0;
            gl.glGenTextures(1, texName, 0);
            gl.glBindTexture(GL_TEXTURE_2D, texName[0]);
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight,0, GL_BGR, GL_UNSIGNED_BYTE, texImg[0]);  // specify texture image
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  //must be specified

            //generate a new object within the scene using object oriented design, passing key properties to init function
            teapot = new Object();
            teapot.initObject(gl, new STeapot(3), ShaderProgram, texName[0]);


            //load and setup texture and define its parameters such as filtering mode
            texImg[1] = loadImage("wood.jpg");
            texName[1] = 1;
            gl.glGenTextures(1, texName, 0);
            gl.glBindTexture(GL_TEXTURE_2D, texName[1]);
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight,0, GL_BGR, GL_UNSIGNED_BYTE, texImg[1]);  // specify texture image
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  //must be specified

            sphere = new Object();
            sphere.initObject(gl, new SSphere(2), ShaderProgram, texName[1]);

            //load and setup texture and define its parameters such as filtering mode
            texImg[2] = loadImage("WelshDragon.jpg");
            texName[2] = 2;
            gl.glGenTextures(1, texName, 0);
            gl.glBindTexture(GL_TEXTURE_2D, texName[2]);
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight,0, GL_BGR, GL_UNSIGNED_BYTE, texImg[2]);  // specify texture image
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  //must be specified


            cube = new Object();
            cube.initObject(gl, new SVolume(1.0f, 1.0f, 1.0f), ShaderProgram2, texName[2]);

            gl.glEnable(GL_DEPTH_TEST);
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
            GL3 gl = drawable.getGL().getGL3(); // Get the GL pipeline object this
            gl.glViewport(x, y, width, height);

            T.initialize();

            //projection
			//T.ortho(-1, 1, -1, 1, -1, 1);  //Default
            // to avoid shape distortion because of reshaping the viewport
            //viewport aspect should be the same as the projection aspect
            if(height<1){height=1;}
            if(width<1){width=1;}
            float a = (float) width / height;   //aspect
            if (width < height) {
                T.ortho(-1, 1, -1/a, 1/a, -1, 1);
                //T.frustum(-1, 1, -1/a, 1/a, 0.1f, 1000);
            }
            else{
                T.ortho(-1*a, 1*a, -1, 1, -1, 1);
                //T.frustum(-1*a, 1*a, -1, 1, 0.1f, 1000);
            }

            //T.perspective(80, a, 0.01f, 1000.0f);

            // Convert right-hand to left-hand coordinate system
            T.rotateX(-90);
            T.reverseZ();

            gl.glUseProgram(ShaderProgram);
            gl.glUniformMatrix4fv( Projection, 1, true, T.getTransformv(), 0 );
            gl.glUseProgram(0);
        }

        public void setupLight() {

        }

        public void renderObject() {

        }

        @Override
        public void mouseDragged(MouseEvent e) {
            //code derived from a CMT107 lab exercise

            int x = e.getX();
            int y = e.getY();

            //left button down, move the object
            if((e.getModifiers() & InputEvent.BUTTON1_MASK) != 0){
                tx += (x-xMouse) * 0.01;
                ty -= (y-yMouse) * 0.01;
                xMouse = x;
                yMouse = y;
            }

            //right button down, rotate the object
            if((e.getModifiers() & InputEvent.BUTTON3_MASK) != 0){
                ry += (x-xMouse) * 1;
                rx += (y-yMouse) * 1;
                xMouse = x;
                yMouse = y;
            }

            //middle button down, scale the object
            if((e.getModifiers() & InputEvent.BUTTON2_MASK) != 0){
                scale *= Math.pow(1.1, (y-yMouse) * 0.5);
                xMouse = x;
                yMouse = y;
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            xMouse = e.getX();
            yMouse = e.getY();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (showSphere)
                showSphere = false;
            else
                showSphere = true;

            System.out.println("Mouse clicked... showSphere = " + showSphere);
        }
        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void mouseEntered(MouseEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void mouseExited(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void keyPressed(KeyEvent e) {
            //support keyboard input for WASD and arrow keys, this is used to control the "camera" allowing the user to move around the scene
            //it doesn't actually transform the camera though, and as more noticeably on VK_W and VK_S it will transform the objects within the
            //scene themselves
            //A switch statement would usually more optimal here but doesn't easily support multiple cases to give the same output
            int key = e.getKeyCode();

            if (key == KeyEvent.VK_W || key == KeyEvent.VK_UP) {
                //achieves a zoom effect by scaling up the surrounding objects
                scale *= Math.pow(1.1, 0.3f * 0.5f);
            }
            else if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
                tx += 0.3f;
            }
            else if (key == KeyEvent.VK_S || key == KeyEvent.VK_DOWN) {
                if (scale > 0.1f)
                    scale *= Math.pow(1.1, -0.3f * 0.5f);
            }
            else if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
                tx -= 0.3f;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //System.out.println("key released");
        }

        @Override
        public void keyTyped(KeyEvent e) {
            //System.out.println("key typed");
        }

        private ByteBuffer loadImage(String imagePath) {
            try {
                ByteBuffer tmpImg = readImage(imagePath);

                return tmpImg;
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }

            return null;
        }

        private ByteBuffer readImage(String filename) throws IOException {

            ByteBuffer imgbuf;
            BufferedImage img = ImageIO.read(new FileInputStream(filename));

            texWidth = img.getWidth();
            texHeight = img.getHeight();
            DataBufferByte datbuf = (DataBufferByte) img.getData().getDataBuffer();
            imgbuf = ByteBuffer.wrap(datbuf.getData());
            return imgbuf;
        }
    }
}
