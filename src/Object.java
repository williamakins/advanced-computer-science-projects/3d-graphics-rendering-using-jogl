import Basic.Transform;
import Basic.Vec3;
import Objects.SObject;
import com.jogamp.opengl.GL3;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL.GL_FLOAT;

public class Object {
    //Transform T = new Transform();

    private int shader = 0;

    //Model parameters
    private int numElements;
    private int vPosition;
    private int vNormal;

    private int idPoint=0, numVAOs = 1;
    private int idBuffer=0, numVBOs = 1;
    private int idElement=0, numEBOs = 1;
    private int[] VAOs = new int[numVAOs];
    private int[] VBOs = new int[numVBOs];
    private int[] EBOs = new int[numEBOs];

    private GL3 gl;

    private int texID;

    public Object() {
    }

    public void initObject(GL3 gl3, SObject object, int shaderProgram, int textureID) {
        gl = gl3;
        shader = shaderProgram;
        texID = textureID;

        gl.glEnable(GL_DEPTH_TEST);

        gl.glEnable(GL_TEXTURE_2D); //must be enabled for old version of OpenGL
        gl.glActiveTexture( GL_TEXTURE0 ); //specify which texture is used
        gl.glBindTexture( GL_TEXTURE_2D, texID );

        //setup default vertex colours, all set to white
        float[] vertexColours ={ //define vertex colours
                1, 1, 1,
                1, 1, 1,
                1, 1, 1,
                1, 1, 1
        };

        //setup and bind the object VBOs to a VAO
        gl.glGenVertexArrays(numVAOs,VAOs,0);
        gl.glBindVertexArray(VAOs[idPoint]);

        //retrive the object vertex and texture data from the SObject base class, for the particular object
        float [] vertexArray = object.getVertices();
        float [] normalArray = object.getNormals();
        float [] textureArray = object.getTextures();
        int [] vertexIndexs =object.getIndices();
        numElements = object.getNumIndices();

        //convert the vertex and texture arrays into floatbuffers, this is required for the glBufferSubData method and doing so
        //also allows the entire data structure to be treated as a single unit rather than needing to deal with the individual elements of an array
        FloatBuffer vertices = FloatBuffer.wrap(vertexArray);
        FloatBuffer normals = FloatBuffer.wrap(normalArray);
        FloatBuffer textures = FloatBuffer.wrap(textureArray);
        FloatBuffer colours = FloatBuffer.wrap(vertexColours);

        gl.glGenBuffers(numVBOs, VBOs,0);
        gl.glBindBuffer(GL_ARRAY_BUFFER, VBOs[idBuffer]);

        // Create an empty buffer with the size we need
        // and a null pointer for the data values
        long vertexSize = vertexArray.length*(Float.SIZE/8);
        long colourSize = vertexColours.length*(Float.SIZE/8);
        long texSize = textureArray.length*(Float.SIZE/8);
        long normalSize = normalArray.length*(Float.SIZE/8);
        gl.glBufferData(GL_ARRAY_BUFFER, vertexSize + colourSize + texSize + normalSize, null, GL_STATIC_DRAW);

        // Load the real data separately.  We put the colors right after the vertex coordinates,
        // so, the offset for colors is the size of vertices in bytes
        gl.glBufferSubData( GL_ARRAY_BUFFER, 0, vertexSize, vertices );
        gl.glBufferSubData( GL_ARRAY_BUFFER, vertexSize, colourSize, colours );
        gl.glBufferSubData( GL_ARRAY_BUFFER, vertexSize + colourSize, texSize, textures );
        gl.glBufferSubData( GL_ARRAY_BUFFER, vertexSize, normalSize, normals );

        IntBuffer elements = IntBuffer.wrap(vertexIndexs);

        gl.glGenBuffers(numEBOs, EBOs,0);
        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOs[idElement]);

        long indexSize = vertexIndexs.length*(Integer.SIZE/8);
        gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize, elements, GL_STATIC_DRAW);



        // Initialize the vertex position attribute in the vertex shader
        int vPosition = gl.glGetAttribLocation( shader, "vPosition" );
        gl.glEnableVertexAttribArray(vPosition);
        gl.glVertexAttribPointer(vPosition, 3, GL_FLOAT, false, 0, 0L);

        // Initialize the vertex color attribute in the vertex shader.
        // The offset is the same as in the glBufferSubData, i.e., vertexSize
        // It is the starting point of the color data
        vNormal = gl.glGetAttribLocation( shader, "vNormal" );
        gl.glEnableVertexAttribArray(vNormal);
        gl.glVertexAttribPointer(vNormal, 3, GL_FLOAT, false, 0, vertexSize);

        // Initialize the vertex color attribute in the vertex shader.
        // The offset is the same as in the glBufferSubData, i.e., vertexSize
        // It is the starting point of the color data
        int vColour = gl.glGetAttribLocation( shader, "vColour" );
        gl.glEnableVertexAttribArray(vColour);
        gl.glVertexAttribPointer(vColour, 3, GL_FLOAT, false, 0, vertexSize);

        int vTexCoord = gl.glGetAttribLocation( shader, "vTexCoord" );
        gl.glEnableVertexAttribArray( vTexCoord );
        gl.glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, false, 0, vertexSize+colourSize);

        // Set the value of the fragment shader texture sampler variable
        //   ("texture") to the the appropriate texture unit. In this case,
        //   zero, for GL_TEXTURE0 which was previously set by calling
        //   glActiveTexture().
        gl.glUseProgram(shader);
        //Get connected with the ModelView matrix in the vertex shader
        Source.ModelView = gl.glGetUniformLocation(shader, "ModelView");
        Source.NormalTransform = gl.glGetUniformLocation(shader, "NormalTransform");
        Source.Projection = gl.glGetUniformLocation(shader, "Projection");

        gl.glUniform1i( gl.glGetUniformLocation(shader, "tex"), 0 );
        gl.glUseProgram(0);
    }

    public void setPosition(Transform T) {
        T.translate(2.5f, 0f, 0f);
    }

    public void renderObject(GL3 gl, Transform T, Vec3 translate, Vec3 scale, Vec3 rotation) {
        gl.glBindVertexArray(VAOs[idPoint]);


        //apply the translation then rotation and then scale to the object each frame, these values are passed in from the
        //source function as it keeps the main code tidy while enabling this ability on the particular objects
        T.translate(translate.getX(), translate.getY(), translate.getZ());

        T.rotateX(rotation.getX());
        T.rotateY(rotation.getY());
        T.rotateZ(rotation.getZ());

        T.scale(scale.getX(), scale.getY(), scale.getZ());

        //enable the shader and bind the textures
        gl.glUseProgram(shader);
        gl.glEnable(GL_TEXTURE_2D);
        gl.glActiveTexture( GL_TEXTURE0 ); //specify which texture is used
        gl.glBindTexture( GL_TEXTURE_2D, texID );

        gl.glUniformMatrix4fv( Source.ModelView, 1, true, T.getTransformv(), 0 );
        gl.glUniformMatrix4fv( Source.NormalTransform, 1, true, T.getInvTransformTv(), 0 );

        //proceed with drawing the 3D objects in the scene, this method is set to GL_TRIANGLES which is fine for basic shapes but
        //complex rendering would benefit from another type such as GL_TRIANGLE_STRIP, therefore reducing the number of
        //vertices within the scene by a significant amount
        gl.glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_INT, 0);
        gl.glBindVertexArray(0);

        gl.glUseProgram(0);
    }

    private void setObjectMatProperties() {

    }
}